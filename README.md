# POS Man

## About

POS (Point Of Sale) web application for handling cashier transaction.

## Frameworks & Application Dependencies
This application is developed using:
* ASP.NET Core 3.1
* Vue.js 2.6.11
* TypeScript 3.8

### ASP.NET Core & Vue.js Component Synchronicity
Vue.js itself is a SPA (Single Page Application) framework, which noticeably you will never see itself reload all the web page whenever user access some page in that website. Configuring SPA routes can be a hassle itself, that is why Accelist propose combining ASP.NET Core & Vue.js framework for more efficient development strategy.

We will use ASP.NET Core to handle the web page routing. Not only that, we can develop a standard Razor Page for developing a simple page.

Then, we will use Vue.js component for creating an interactive UI components, like a datagrid that come with its filter fields, which whenever user submit a new value into its filter flieds, only the datagrid component will reload the data, not the entire web page.

### ASP.NET Core NuGet Dependencies
Below are the list of NuGet dependencies:
* Swashbuckle.AspNetCore v5.0.0

### ASP.NET Core Default Files Changes:
The default ASP.NET Core Web Application template include standard JS & CSS libraries, such as Bootstrap and jQuery. Since we will use NPM as our package manager for front-end files, we can delete unnecessary folders/files from the default template. You can compare the changes by creating a new ASP.NET Core Web Application project, and compare the changes between the new project and this project.

Below are the deleted folders/files:
* `wwwroot/css/site.css`
* `wwwroot/js/site.js`
* `lib` folder and its contents

> The `site.css` codes were moved into `client/css/site.scss`.

## Development Tools
Below are the list of development tools that are needed for developing this application:
* .NET Core SDK 3.1
* Node.js 12
* instapack 8
* TypeScript 3.8

> You can run instapack using `npx` command, for example: `npx ipack`. If you want to run instapack as your global package, please use the same version as one that included in `package.json` file.

## Installation & Setup
To run this application on your server, you have to install:
* .NET Core runtime 3.1

By default, ASP.NET Core application will run its own reverse proxy server, **Kestrel**, by running this following command: `dotnet POSMan.Web.dll`. Alternatively, you can use IIS or other reverse proxy server for running this application.