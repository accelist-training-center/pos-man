﻿using POSMan.Web.Data.Master;
using POSMan.Web.Models.Commons;
using POSMan.Web.Models.Master.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSMan.Web.Services.Master.Product
{
    public class GetProductService
    {
        private readonly ProductData ProductData;

        public GetProductService(ProductData productData)
        {
            this.ProductData = productData;
        }

        public Models.Master.Product.FilterModel Filter { get; set; }

        public DataGridModel<ProductModel> GetProducts()
        {
            var filter = new FilterModel();

            if (this.Filter != null)
            {
                filter = this.Filter;
            }

            var products = this.ProductData.Products;

            if (string.IsNullOrEmpty(filter.Pn) == false)
            {
                products = products
                    .Where(Q => Q.Name.ToLower().StartsWith(filter.Pn.ToLower()))
                    .ToList();
            }

            if (string.IsNullOrEmpty(filter.Pd) == false)
            {
                products = products
                    .Where(Q => Q.Description.ToLower().StartsWith(filter.Pd.ToLower()))
                    .ToList();
            }

            /*
             * WARNING! The impresentation of number range can be vary, depends on the requirements.
             * So, suit yourself.
             */
            if (filter.PpFrom != null && filter.PpTo != null)
            {
                products = products
                    .Where(Q => Q.Price >= filter.PpFrom.Value && Q.Price <= filter.PpTo.Value)
                    .ToList();
            }
            else if (filter.PpFrom != null)
            {
                var maxPrice = this.ProductData.Products.Max(Q => Q.Price);
                products = products
                    .Where(Q => Q.Price >= filter.PpFrom.Value && Q.Price <= maxPrice)
                    .ToList();
            }
            else if (filter.PpTo != null)
            {
                var minPrice = this.ProductData.Products.Min(Q => Q.Price);
                products = products
                    .Where(Q => Q.Price >= minPrice && Q.Price <= filter.PpTo.Value)
                    .ToList();
            }

            if (filter.IDsc == true)
            {
                switch (filter.Ob)
                {
                    case "Pp":
                        products = products
                            .OrderByDescending(Q => Q.Price)
                            .ToList();
                        break;
                    case "Pd":
                        products = products
                            .OrderByDescending(Q => Q.Description)
                            .ToList();
                        break;
                    default:
                        products = products
                            .OrderByDescending(Q => Q.Name)
                            .ToList();
                        break;
                }
            }
            else
            {
                switch (filter.Ob)
                {
                    case "Pp":
                        products = products
                            .OrderBy(Q => Q.Price)
                            .ToList();
                        break;
                    case "Pd":
                        products = products
                            .OrderBy(Q => Q.Description)
                            .ToList();
                        break;
                    default:
                        products = products
                            .OrderBy(Q => Q.Name)
                            .ToList();
                        break;
                }
            }

            var totalData = products.Count;

            if (filter.P < 1)
            {
                filter.P = 1;
            }

            products = products
                .Skip((filter.P - 1) * filter.Ipp)
                .Take(filter.Ipp)
                .ToList();

            var dataGrid = new DataGridModel<ProductModel>
            {
                Data = products,
                TotalData = totalData
            };

            return dataGrid;
        }
    }
}
