﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using POSMan.Web.Models.Commons;
using POSMan.Web.Models.Master.Product;
using POSMan.Web.Services.Master.Product;

namespace POSMan.Web.API
{
    [Route("api/v1/product")]
    public class ProductController : Controller
    {
        private readonly GetProductService GetProductService;

        public ProductController(GetProductService getProductService)
        {
            this.GetProductService = getProductService;
        }

        [HttpGet(Name = "GetProducts")]
        public ActionResult<DataGridModel<ProductModel>> Get([FromQuery] Models.Master.Product.FilterModel filter)
        {
            this.GetProductService.Filter = filter;

            var dataGrid = this.GetProductService.GetProducts();

            return dataGrid;
        }
    }
}
