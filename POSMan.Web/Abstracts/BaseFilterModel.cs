﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSMan.Web.Interfaces
{
    /// <summary>
    /// Abstrace class for defining the standard of filter model class.
    /// </summary>
    public abstract class BaseFilterModel
    {

        /// <summary>
        /// Order by.
        /// </summary>
        public string Ob { get; set; }

        /// <summary>
        /// Is sort by descending.
        /// </summary>
        public bool IDsc { get; set; }

        /// <summary>
        /// Item per page.
        /// </summary>
        public int Ipp { get; set; }

        /// <summary>
        /// The selected page index.
        /// </summary>
        public int P { get; set; }
    }
}
