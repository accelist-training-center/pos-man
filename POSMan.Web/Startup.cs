using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using POSMan.Web.Data.Master;
using POSMan.Web.Services.Master.Product;

namespace POSMan.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            Configuration = configuration;
            this.WebHostEnvironment = webHostEnvironment;
        }

        public IConfiguration Configuration { get; }

        public IWebHostEnvironment WebHostEnvironment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var mvcBuilder = services.AddRazorPages();

            if (this.WebHostEnvironment.IsDevelopment() == true)
            {
                /*
                 * From ASP.NET Core 3.0, the default behavior of Razor Page compilation has been changed.
                 * From now on, if you want to enable the automatic compilation on Razor Page changes during runtime, you must
                 * call AddRazorRuntimeCompilation() method.
                 * Reference: https://docs.microsoft.com/en-us/aspnet/core/mvc/views/view-compilation?view=aspnetcore-3.1
                 */
                mvcBuilder.AddRazorRuntimeCompilation();
            }

            // Default ASP.NET Core Web Application project doesn't include this code.
            services.AddControllers();

            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "POS Man API",
                    Version = "v1"
                });
            });

            services.AddSingleton<ProductData>();

            services.AddTransient<GetProductService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseSwagger();

            // Automatically create a Swagger UI. Can be used for OpenAPI documentation.
            app.UseSwaggerUI(config =>
            {
                config.SwaggerEndpoint("/swagger/v1/swagger.json", "POS Man V1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();

                // Default ASP.NET Core Web Application project doesn't include this code.
                endpoints.MapControllers();
            });
        }
    }
}
