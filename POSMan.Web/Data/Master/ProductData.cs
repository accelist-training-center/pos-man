﻿using POSMan.Web.Models.Master.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSMan.Web.Data.Master
{
    public class ProductData
    {
        public List<ProductModel> Products { get; set; } = new List<ProductModel>
        {
            new ProductModel
            {
                ProductId = Guid.NewGuid(),
                Name = "Nintendo Switch",
                Price = 4_500_000,
                Description = "The ultimate Nintendo's mobile gaming console"
            },
            new ProductModel
            {
                ProductId = Guid.NewGuid(),
                Name = "PS4 Pro",
                Price = 5_000_000,
                Description = "The ultimate Sony's gaming console"
            },
            new ProductModel
            {
                ProductId = Guid.NewGuid(),
                Name = "Xbox One",
                Price = 4_700_000,
                Description = "The ultimate Microsoft's gaming console"
            },
            new ProductModel
            {
                ProductId = Guid.NewGuid(),
                Name = "Oculus Rift",
                Price = 10_000_000,
                Description = "VR Supercharged!"
            },
            new ProductModel
            {
                ProductId = Guid.NewGuid(),
                Name = "NVIDIA GEFORCE RTX 2070",
                Price = 9_000_000,
                Description = "Need more GPU in your life?"
            },
            new ProductModel
            {
                ProductId = Guid.NewGuid(),
                Name = "Ultimate PC Gaming Machine",
                Price = 50_000_000,
                Description = "The ultimate all-in-one PC gaming machine"
            }
        };
    }
}
