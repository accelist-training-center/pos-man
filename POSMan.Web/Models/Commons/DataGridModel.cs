﻿using POSMan.Web.Models.Master.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSMan.Web.Models.Commons
{
    public class DataGridModel<T>
    {
        public List<T> Data { get; set; }

        public int TotalData { get; set; }
    }
}
