﻿using POSMan.Web.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace POSMan.Web.Models.Master.Product
{
    public class FilterModel : BaseFilterModel
    {
        /// <summary>
        /// Product Name filter.
        /// </summary>
        public string Pn { get; set; }

        /// <summary>
        /// Price from range filter.
        /// </summary>
        public decimal? PpFrom { get; set; }

        /// <summary>
        /// Price to range filter.
        /// </summary>
        public decimal? PpTo { get; set; }

        /// <summary>
        /// Product Description filter.
        /// </summary>
        public string Pd { get; set; }

        public decimal Pp { get; set; }
    }
}
