FROM node:12-slim AS instapack

# Run shell command.
# In this case, it will run instapack installation as a global package.
RUN npm install -g instapack
COPY . /src

# Change directory or cd command in Docker.
# Will create a new directory if not exist yet during build.
# Change working directory to POSMan.Web folder, which package.json exists in this location.
WORKDIR /src/POSMan.Web
RUN ipack

# Pull the .NET Core SDK to build / compile our application in Docker Engine.
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 AS build

COPY --from=instapack /src /src

WORKDIR /src/POSMan.Web

RUN dotnet restore
RUN dotnet publish -c Release -o out

# Build runtime image.
# If we are declare another FROM, then the focus image will be changed into the next one.
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1
WORKDIR /app
COPY --from=build /src/POSMan.Web/out .

# Will run "dotnet POSMan.Web.dll" when the container run using this image.
ENTRYPOINT [ "dotnet", "POSMan.Web.dll" ]